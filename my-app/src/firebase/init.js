// Your web app's Firebase configuration
import * as firebase from 'firebase/app'
import 'firebase/firestore'

// import firebase from 'firebase'
// import firestore from 'firebase/firestore'

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyB70qR1gYbk_cpL4IZDdaVY3oQXABsk_Jw",
  authDomain: "tunnel-insight-270606.firebaseapp.com",
  databaseURL: "https://tunnel-insight-270606.firebaseio.com",
  projectId: "tunnel-insight-270606",
  storageBucket: "tunnel-insight-270606.appspot.com",
  messagingSenderId: "617780490793",
  appId: "1:617780490793:web:ce6367610c0b81a46f6238"
};
  // Initialize Firebase
  const firebaseApp = firebase.initializeApp(firebaseConfig);

// firebaseApp.firestore().settings ({timestampsInSnapshots:true })

 // firebase.analytics();

  export default firebaseApp.firestore() ;
  
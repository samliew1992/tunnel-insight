import Vue from 'vue'
import Router from 'vue-router'
// import Gmap from '@/components/home/Gmap'
import signup from '@/components/auth2/signup'
import login from '@/components/auth2/login'
// import viewprofile from '@/components/auth2/profile/viewprofile'
import home from '@/components/home/home'
import firebase from 'firebase'
import TBMLiveData from '@/components/TBMLiveData/TBMLiveData'



Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [

    
    {
      path:'/signup',
      name:'signup',
      component: signup
    },
    {
      path:'/login',
      name:'login',
      component: login
    },

    {
      path:'/home',
      name:'home',
      component: home,
      meta:{
        requiresAuth: true
      }
    },

    {
      path:'/TBMLiveData',
      name:'TBMLiveData',
      component: TBMLiveData,
      meta:{
        requiresAuth: true
    
      }
    }

  ]
})

router.beforeEach((to, from, next) => {
  //check to see if route required auth 
  if(to.matched.some(rec => rec.meta.requiresAuth)){
    //check auth state of user 
    let user = firebase.auth().currentUser
    if(user){
      // user signed in, proceed to route 
      next()
    }else {
      // no user signed in, redirect to login
      next({ name:'login'})
    }
  }else{
    next()
  }
})

export default router
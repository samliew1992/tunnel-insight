import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import 'vue-material-design-icons/styles.css'
import firebase from 'firebase'



Vue.config.productionTip = false

let app= null;

//wait for firebase auth to init before creating the app 
firebase.auth().onAuthStateChanged(()=> {
  //init app if not already created 
  if(!app){
    app = new Vue({
      el: '#app',
      router,
      vuetify,
      components: { App },
      template: '<App/>',
      render: h => h(App)
    }).$mount('#app')
  }

})
/* eslint-disable no-new */